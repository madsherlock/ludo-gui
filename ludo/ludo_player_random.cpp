#include "ludo_player_random.h"
//#include <iostream>

ludo_player_random::ludo_player_random():
    pos_start_of_turn(16),
    pos_end_of_turn(16),
    dice_roll(0),
    rd(),
    gen(rd())
{
}

ludo_player_random::ludo_player_random(unsigned int custom_seed)
    :
      pos_start_of_turn(16),
      pos_end_of_turn(16),
      dice_roll(0),
      rd(),
      gen(custom_seed)
{

}

int ludo_player_random::make_decision(){
    std::vector<int> valid_moves;
    if(dice_roll == 6){
        for(int i = 0; i < 4; ++i){
            if(pos_start_of_turn[i]<0){
                valid_moves.push_back(i);
            }
        }
    }
    for(int i = 0; i < 4; ++i){
        if((pos_start_of_turn[i]>=0) && (pos_start_of_turn[i] != 99)){
            valid_moves.push_back(i);
        }
    }
    if(valid_moves.empty()){ //there are only impossible moves left, so return any piece not in goal
        for(int i = 0; i < 4; ++i){
            if(pos_start_of_turn[i] != 99){
                return i; //first piece not in goal is returned (NOT MOVED ANYWAY)
                //valid_moves.push_back(i);
                //std::cout << i << ',' << std::flush;
            }
        }
    }
    std::uniform_int_distribution<> piece(0, valid_moves.size()-1);
    int select = piece(gen);
    return valid_moves[select];
}

void ludo_player_random::start_turn(positions_and_dice relative){
    pos_start_of_turn = relative.pos;
    dice_roll = relative.dice;
    int decision = make_decision();
    emit select_piece(decision);
}

void ludo_player_random::post_game_analysis(std::vector<int> relative_pos){
    pos_end_of_turn = relative_pos;
    bool game_complete = true;
    for(int i = 0; i < 4; ++i){
        if(pos_end_of_turn[i] != 99){
            game_complete = false;
        }
    }
    emit turn_complete(game_complete);
}
