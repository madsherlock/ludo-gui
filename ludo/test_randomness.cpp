#include "test_randomness.h"
#include <QApplication>
#include "game.h"
#include <vector>
#include "ludo_player_random.h"
#include "positions_and_dice.h"
#include <iostream>
#include <iomanip>

#include "dialog.h"


Q_DECLARE_METATYPE( positions_and_dice )


using namespace std;

test_randomness::test_randomness()
{

}

void test_randomness::test_four_random_players(const unsigned long num_games) const
{
    cout << "Testing four random players..." << endl;
    int argc=1;
    char **argv=nullptr;
    QApplication a(argc,argv);
    qRegisterMetaType<positions_and_dice>();
    ludo_player_random p1(10), p2(34), p3(33), p4(47);
    game g;
     g.setGameDelay(0); //if you want to see the game, set a delay



//    g.setGameDelay(10000); //if you want to see the game, set a delay
//    Dialog w;
//    QObject::connect(&g,SIGNAL(update_graphics(std::vector<int>)),&w,SLOT(update_graphics(std::vector<int>)));
//    QObject::connect(&g,SIGNAL(set_color(int)),                   &w,SLOT(get_color(int)));
//    QObject::connect(&g,SIGNAL(set_dice_result(int)),             &w,SLOT(get_dice_result(int)));
//    QObject::connect(&g,SIGNAL(declare_winner(int)),              &w,SLOT(get_winner()));
//    QObject::connect(&g,SIGNAL(close()),&a,SLOT(quit()));
//    w.show();



    QObject::connect(&g,SIGNAL(close()),&a,SLOT(quit()));

    //set up for each player
    QObject::connect(&g, SIGNAL(player1_start(positions_and_dice)),&p1,SLOT(start_turn(positions_and_dice)));
    QObject::connect(&p1,SIGNAL(select_piece(int)),                &g, SLOT(movePiece(int)));
    QObject::connect(&g, SIGNAL(player1_end(std::vector<int>)),    &p1,SLOT(post_game_analysis(std::vector<int>)));
    QObject::connect(&p1,SIGNAL(turn_complete(bool)),              &g, SLOT(turnComplete(bool)));

    QObject::connect(&g, SIGNAL(player2_start(positions_and_dice)),&p2,SLOT(start_turn(positions_and_dice)));
    QObject::connect(&p2,SIGNAL(select_piece(int)),                &g, SLOT(movePiece(int)));
    QObject::connect(&g, SIGNAL(player2_end(std::vector<int>)),    &p2,SLOT(post_game_analysis(std::vector<int>)));
    QObject::connect(&p2,SIGNAL(turn_complete(bool)),              &g, SLOT(turnComplete(bool)));

    QObject::connect(&g, SIGNAL(player3_start(positions_and_dice)),&p3,SLOT(start_turn(positions_and_dice)));
    QObject::connect(&p3,SIGNAL(select_piece(int)),                &g, SLOT(movePiece(int)));
    QObject::connect(&g, SIGNAL(player3_end(std::vector<int>)),    &p3,SLOT(post_game_analysis(std::vector<int>)));
    QObject::connect(&p3,SIGNAL(turn_complete(bool)),              &g, SLOT(turnComplete(bool)));

    QObject::connect(&g, SIGNAL(player4_start(positions_and_dice)),&p4,SLOT(start_turn(positions_and_dice)));
    QObject::connect(&p4,SIGNAL(select_piece(int)),                &g, SLOT(movePiece(int)));
    QObject::connect(&g, SIGNAL(player4_end(std::vector<int>)),    &p4,SLOT(post_game_analysis(std::vector<int>)));
    QObject::connect(&p4,SIGNAL(turn_complete(bool)),              &g, SLOT(turnComplete(bool)));

    //obtain wins
    QObject::connect(&g,SIGNAL(declare_winner(int)),                this, SLOT(declare_winner(int)));

    cout << setw(10) << "runs" << setw(10) << "player 1" << setw(10) << "player 2" << setw(10) << "player 3" << setw(10) << "player 4" << endl;
    for(unsigned long i = 0; i < num_games; ++i){
        g.start();
        a.exec();
        g.reset();
        if(!((i+1)%((num_games)/100))) {
            cout << setw(10) << i+1 << fixed << setprecision(4);
            for(auto &wc:win_counts)
                cout << setw(10) << ((static_cast<double>(wc))/(static_cast<double>(i+1)));
            cout << endl;
        }
    }

    cout << "Test complete" << endl;
}

void test_randomness::declare_winner(int winner) {
    ++(win_counts.at(winner));
}
