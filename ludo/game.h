#ifndef GAME_H
#define GAME_H

#include <vector>//
#include <random>//
#include <QThread>//


#include "positions_and_dice.h"

// static int global_color = 5;

class game : public QThread
{
    Q_OBJECT
private:
    bool game_complete;
    bool turn_complete;
    unsigned int game_delay;
    positions_and_dice relative;
    int dice_result;
    std::random_device rd;
    std::mt19937 gen;
    std::uniform_int_distribution<> distr_dice;
    std::uniform_int_distribution<> distr_color;
    bool valid_move_exists;
    std::vector<int> relativePosition();
    /**
     * @brief isStar Checks whether a board position is a star
     * @param index Board position wrt. a home globe.
     * @return 7 if it's a star right before a goal zone. 6 if it's one of the other stars. 0 if it's not a star.
     */
    int isStar(int index);
    bool isGlobe(int index);
    int isOccupied(int index); //see if it is occupied and return the piece number
    int rel_to_fixed(int relative_piece_index);
    void send_them_home(int index);
    void move_start(int fixed_piece);
    int next_turn(unsigned int delay);
    static void msleep(unsigned long msecs){
        if(msecs>0) QThread::msleep(msecs); //msleep suspends thread no matter what msecs is
    }
    const std::vector<size_t> get_valid_moves(const positions_and_dice& pos_start_of_turn) const;

public:
    int color;
    std::vector<int> player_positions;
    void rollDice(){
        dice_result = distr_dice(gen);
    }
    int getDiceRoll() {return dice_result; }
    game(unsigned long custom_seed);

    game();
    void setGameDelay(unsigned int mili_seconds){ game_delay = mili_seconds; }
    void reset();
signals:
    void player1_start(positions_and_dice);
    void player2_start(positions_and_dice);
    void player3_start(positions_and_dice);
    void player4_start(positions_and_dice);

    void player1_end(std::vector<int>);
    void player2_end(std::vector<int>);
    void player3_end(std::vector<int>);
    void player4_end(std::vector<int>);

    void update_graphics(std::vector<int>);
    void set_color(int);
    void set_dice_result(int);
    void declare_winner(int);
    void close();

public slots:
    void turnComplete(bool win);
    void movePiece(int relative_piece); //check game rules
protected:
    void run();
};

#endif // GAME_H
