#ifndef TEST_RANDOMNESS_H
#define TEST_RANDOMNESS_H
#include <QObject>
#include <array>

class test_randomness : public QObject
{
    Q_OBJECT
public:
    test_randomness();

    std::array<size_t,4> win_counts;
    void test_four_random_players(const unsigned long num_games=100000) const;
public slots:
    void declare_winner(int winner);
};

#endif // TEST_RANDOMNESS_H
